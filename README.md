# Ethical Channel

ATGroup's GlobaLeaks implementation source files.

## License

**Copyright (C) 2022 Aula Tecnomedia S.L.U.**

The source code in this repository is released under the Affero General Public License, either version 3, or (at your option) any later versions released by the Free Software Foundation.

You can get a copy of this license from the file `LICENSE` or from [gnu.org](https://www.gnu.org/licenses/agpl).

Please note that if you use the source files provided in this repository or substantial portions of this, you have to release it under a compatible license as stated before.

## Getting started

To use our sources to make your own implementation, you first need to set up a [GlobaLeaks](https://www.globaleaks.org/) instance.

After this, you are free to modify the source files provided in this repository in acordance with the license and, inside a GlobaLeaks administrator account, go to `Site settings` > `Theme customization` and upload the following files:

* `custom_stylesheet.css` to `CSS`
* `custom_script.js` to `JavaScript`

Since GlobaLeaks runs on JavaScript sessions, to see the changes you have to restart this session clicking on the GlobaLeaks icon on the top left of the page.

[Since a recent update](https://github.com/orgs/globaleaks/discussions/3416), JavaScript files can't be changed on the web, and need to be uploaded directly to the server.

