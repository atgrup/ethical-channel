/*
	Copyright (C) 2022 AULA TECNOMEDIA S.L.U.

	This source is released under the Affero GNU Public License, as
	published by the Free Software Foundation, either version 3 or
	(at your option) any later version.

	See <https://www.gnu.org/licenses/agpl>.
*/

/*
	Sections bitmask
	(ctrl+f section name)
*/
let sections = 0b00000000
/*               |||||||| RECEIPT_SECTION
                 ||||||| UNUSED
                 |||||| UNUSED
                 ||||| UNUSED
                 |||| UNUSED
                 ||| UNUSED
                 || UNUSED
                 | UNUSED
*/


GL.mockEngine.addMock("*", "#WhistleblowerLoginQuestion", function() {
  return "<hr>"
}, "add-before")

GL.mockEngine.addMock("*", "#AttributionClause", function() {
	return `
		Powered by <a href="https://www.globaleaks.org/">GlobaLeaks</a>
		and <a href="https://atgroup.es/">ATGroup</a>
	`
})

// RECEIPT_SECTION
GL.mockEngine.addMock("*", "#Receipt", function(el) {

	if (sections & 0b00000001) return

	let p = document.createElement("p");

	switch (GL.language) {

		case "ca":
			p.innerHTML = `
				Aquest codi unic i exclusiu és amb el que Vosté podrà consultar l'estat de la tramitació a l'apartat
				CONSULTAR TRAMITACIÓ de la pàgina web www.canaletico.com.
				<br><br>
				<h4>No perdi ni cedeixi a tercers aquest codi ja que no es generarà cap copia del mateix ni s'enviarà
				cap correu electrónic (en cas d'haver-se identificat) per qüestions de seguretat.</h4>
			`
			break

		case "de":
			p.innerHTML = ``
			break

		case "en":
			p.innerHTML = `
				With this unique code You can follow and manage the status of Your report. To do this, access the section
				CHECK REPORT on the website www.canaletico.com.
				<br><br>
				<h4>Do not lose or grant this code to third parties, no copies will be generated and it will not be sent to
				any email address (in case of choosing to identify onself) for security reasons.<h4>
			`
			break

		case "es":
			p.innerHTML = `
				Este código único y exclusivo es con el que Usted va a poder consultar el estado de la tramitación
				a través del apartado CONSULTAR TRAMITACIÓN de la página web www.canaletico.com.
				<br><br>
				<h4>No pierda ni ceda a terceros este código ya que no se va a generar
				ninguna copia del mismo ni se le va a enviar ningún correo electrónico (en caso de haberse identificado)
				por cuestiones de seguridad.</h4>
			`
			break

		case "fr":
			p.innerHTML = `
				C’est avec ce code unique et exclusive que vous allez pouvoir consulter l’état de la démarche à travers
				du paragraphe CONSULTER LA DÉMARCHE de la page web www.canaletico.com.
				<br><br>
				<h4>Ne perdez pas le code ou ne le cédez pas à un/des tiers car  ce dernier ne sera envoyée à aucune
				adresse électronique (dans le cas de s’être identifié) et aucune copie ne sera générée, pour des
				questions de sécurité.</h4>
			`
			break

		case "it":
			p.innerHTML = `
				Questo codice unico ed esclusivo è con il quale potrai verificare lo stato del trattamento attraverso
				la sezione CONSULTAZIONE ELABORAZIONE del sito www.canaletico.com.
				<br><br>
				<h4>Non perdere o trasferire questo codice a terzi poiché non ne verrà generata alcuna copia né ti verrà
				inviata alcuna email (se ti sei identificato) per motivi di sicurezza.</h4>
			`
			break

		case "nl":
			p.innerHTML = ``
			break

		case "pl":
			p.innerHTML = ``
			break

		case "pt_PT":
			p.innerHTML = ``
			break

		case "ja":
			p.innerHTML = `
				このコードは貴方が今回通報した件に対する専用コードです。調査・手続きの進捗状況を確認するには、
				ウェブサイトwww.canaletico.comの「手続き状況確認」のセクションからこの専用コードを使ってアクセスしてください。
				<br><br>
				<h4>機密保持の観点から、このコードは再発行不可であり、また記名で通報された方にメールで送られることもありません。
				当コードを紛失しないよう、また第三者に教えたりすることのないよう、十分にご注意ください。</h4>
			`
			break

		case "zh_CN":
			p.innerHTML = `
				使用这个唯一的代码，您可跟踪和管理报告状态。为此，请访问
				www.canaletico.com 网站查询检查报告。
				<br><br>
				出于安全因素，不要将此代码丢失或授予第三方，避免生成任何副
				本，也不要将其发送到任何电子邮件地址(如有默认请自行识别)。
			`
			break


	}
	
	el.parentNode.insertBefore(p, el)
	sections = sections | 0b00000001
})

